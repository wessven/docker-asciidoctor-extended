= Example Slides
John Doe <john.doe@example.com>
:lang: en_gb
:keywords: asciidoc, asciidoctor, revealjs, asciidoctor-diagram, vega, +
	vega-lite, example, demo
:ascii-ids:
:icons: font
:numbered!:
:bibtex-style: apa
:bibtex-file: references.bib
:revealjsdir: https://cdn.jsdelivr.net/npm/reveal.js@3.9.2
:revealjs_slideNumber: c/t
:revealjs_theme: solarized
:revealjs_transition: fade
:source-highlighter: highlightjs
//:highlightjs-theme: solarized_light

== Normal Slide

* Lorem ipsum dolor sit amet
* Consectetur adipiscing elit cite:[leff]
* Sed di eiusmod tempor incididunt et dolore magna aliqua

== Slide with Chart

.A bar chart of my favourite pies.  This chart was dynamically generated.
[vegalite, Figures/bar-chart-of-favourite-pies, svg, width=640]
....
include::Figures/bar-chart-of-favourite-pies.json[]
....

[transition=zoom]
== Slide with Source Code

[source, python, linenums, highlight=2..3]
----
def count_occurrences_in_column(matrix: List[List], col: int) -> Dict:
    ''' Count how many times each element occurs in a "column" in a 2D
    matrix. ''' # <1>

    return {
            x: sum(1 for l in matrix if l[col] == x)
            for y in matrix for n, x in enumerate(y) if n == col
        }
----
<1> It's always important to document your code!

[bibliography]
== References

bibliography::[]

////
vim: spell:spelllang=en_gb:syntax=asciidoc:textwidth=79:linespace=3:
////
