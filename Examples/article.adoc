= Example Document
John Doe <john.doe@example.com>
:lang: en_gb
:keywords: asciidoc, asciidoctor, bibtex, asciidoctor-bibtex, +
	asciidoctor-diagram, vega, vega-lite, example, demo
:ascii-ids:
:icons: font
:numbered!:
:bibtex-style: apa
:bibtex-file: references.bib
:source-highlighter: rouge

:fn-1: footnote:[Facilisi etiam dignissim diam quis enim lobortis +
	scelerisque. Aliquet risus feugiat in ante metus dictum at. Et +
	sollicitudin ac orci phasellus egestas tellus rutrum. Tortor +
	posuere ac ut consequat.]
:url-vegalite: https://vega.github.io/vega-lite/[Vega-Lite]

A demonstration of using `asciidoctor-diagram` to generate graphs using
{url-vegalite}, and of using `asciidoctor-bibtex` to generate bibliographic
information using BibTeX.

== Introduction

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua{fn-1}.

== Findings

Accumsan sit amet nulla facilisi morbi tempus. Sodales neque sodales ut etiam
sit amet. Eu lobortis elementum nibh tellus molestie. Pharetra vel turpis nunc
eget. Ut tortor pretium viverra suspendisse. Malesuada fames ac turpis egestas
sed. Aenean euismod elementum nisi quis eleifend quam. Nascetur ridiculus mus
mauris vitae. Ac ut consequat semper viverra nam libero justo. At augue eget
arcu dictum varius duis. Quam lacus suspendisse faucibus interdum. Ac odio
tempor orci dapibus ultrices. A diam maecenas sed enim ut.


////
Note that the two "Figures" directories below are potentially different.  The
first refers to a new subdirectory which will be created at the location of
the output directory, which the second one refers to a directory at the input
location.
////
.A pie chart of my favourite bars.  This chart was dynamically generated.
[vegalite, Figures/pie-chart-of-favourite-bars, svg, width=600, align=center]
....
include::Figures/pie-chart-of-favourite-bars.json[]
....

Ornare lectus sit amet est placerat in egestas erat. Integer quis auctor elit
sed vulputate mi sit. Phasellus vestibulum lorem sed risus ultricies tristique
nulla aliquet enim. Dui ut ornare lectus sit amet est placerat. Cursus turpis
massa tincidunt dui ut. Neque convallis a cras semper auctor neque vitae. Diam
sit amet nisl suscipit adipiscing bibendum est ultricies. Leo a diam
sollicitudin tempor id eu. Rutrum tellus pellentesque eu tincidunt tortor
aliquam. Massa sapien faucibus et molestie ac. Risus feugiat in ante metus
dictum at. Volutpat blandit aliquam etiam erat velit scelerisque in dictum non.
Felis eget nunc lobortis mattis. Sed id semper risus in hendrerit cite:[singh].

[source, python, linenums, highlight=2..3]
----
def count_occurrences_in_column(matrix: List[List], col: int) -> Dict:
    ''' Count how many times each element occurs in a "column" in a 2D
    matrix. ''' # <1>

    return {
            x: sum(1 for l in matrix if l[col] == x)
            for y in matrix for n, x in enumerate(y) if n == col
        }
----
<1> It's always important to document your code!

== Conclusion

Ultricies mi quis hendrerit dolor magna eget est lorem. Odio ut sem nulla
pharetra. Cursus in hac habitasse platea dictumst. Egestas quis ipsum
suspendisse ultrices gravida dictum fusce ut. Dictum non consectetur a erat nam
at. Ante in nibh mauris cursus. Tincidunt eget nullam non nisi est sit. A diam
sollicitudin tempor id eu nisl. Aliquet bibendum enim facilisis gravida neque
convallis a cras semper. Ligula ullamcorper malesuada proin libero nunc
consequat interdum. Nulla facilisi morbi tempus iaculis. A iaculis at erat
pellentesque. Et netus et malesuada fames ac. Risus commodo viverra maecenas
accumsan lacus vel. At consectetur lorem donec massa. Diam quis enim lobortis
scelerisque fermentum dui.

[bibliography]
== References

bibliography::[]

////
vim: spell:spelllang=en_gb:syntax=asciidoc:textwidth=79:linespace=3:
////
